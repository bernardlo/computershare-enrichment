﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using NLog;

namespace ComputerShare_Enrichment {
    public class UploadFileCreator {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private const string divisionId = "PEN.DIV.B2C";

        private const string datafeedCode = "DF.CPU";

        private class SfdcDateConverter : DefaultTypeConverter {
            public override string ConvertToString(object value, IWriterRow row, MemberMapData memberMapData) {
                if (value == null) {
                    return string.Empty;
                } else {
                    var v = (DateTime)value;
                    return v.ToString("yyyy-MM-dd");
                }
            }
        }

        private class SfdcCurrencyConverter : DefaultTypeConverter {
            public override string ConvertToString(object value, IWriterRow row, MemberMapData memberMapData) {
                if (value == null) {
                    return string.Empty;
                } else {
                    var v = (decimal)value;
                    return Convert.ToInt64(v).ToString();
                }
            }
        }

        public void CreateEnrichedDataFile (string filePath, IEnumerable<EnrichedData> records) {
            if (filePath == null) throw new ArgumentNullException("filePath");
            if (records == null) throw new ArgumentNullException("data");
            if (records.Count() == 0) throw new ArgumentException("data has no records");
            using (var writer = new StreamWriter(filePath))
            using (var csv = new CsvWriter(writer)) {
                csv.WriteRecords(records);
            }
            logger.Info("Wrote all enriched data with {0} records.", records.Count());
        }

        #region Accounts

        public class PractifiAccount {
            public string LastName { get; set; }
            public string cloupra__Business_Number__c { get; set; }
            public string MailingStreet { get; set; }
            public string MailingCity { get; set; }
            public string MailingState { get; set; }
            public string MailingPostalCode { get; set; }
            public string MailingCountry { get; set; }
            public string Phone { get; set; }
            public string cloupra__Batch__c { get; set; }
            public string Email { get; set; }
            public string cloupra__Person_Segment__c { get; set; }
            public string cloupra__External_Id__c { get; set; }
            public string MobilePhone { get; set; }
            public string cloupra__Primary_Division__c { get; set; }
            public string InvestorAccountType__c { get; set; }
            public string LeadSource { get; set; }
            public string cloupra__Stage__c { get; set; }
            public DateTime? Birthdate { get; set; }
            public bool HasOptedOutOfEmail { get; set; }
            public string BrokerName__c { get; set; }
            public bool IsAdvised__c { get; set; }
            public string Platform__c { get; set; }
            public string cloupra__Addressee_Name__c { get; set; }
            public string HomePhone { get; set; }
            public bool DoNotCall { get; set; }
            public string ComputershareCommunicationPreference__c { get; set; }
        }

        public class PractifiAccountMap : ClassMap<PractifiAccount> {
            public PractifiAccountMap () {
                Map(m => m.LastName).Index(0);
                Map(m => m.cloupra__Business_Number__c).Index(1);
                Map(m => m.cloupra__External_Id__c).Index(2);
                Map(m => m.LeadSource).Index(3);
                Map(m => m.cloupra__Primary_Division__c).Index(4).Name("cloupra__Primary_Division__r.cloupra__External_Id__c");
                Map(m => m.cloupra__Person_Segment__c).Index(5).Name("cloupra__Person_Segment__r.cloupra__Code__c");
                Map(m => m.InvestorAccountType__c).Index(6);
                Map(m => m.cloupra__Stage__c).Index(7).Name("cloupra__Stage__r.cloupra__Code__c");
                Map(m => m.Birthdate).Index(8).TypeConverter<SfdcDateConverter>();
                Map(m => m.BrokerName__c).Index(9);
                Map(m => m.IsAdvised__c).Index(10);
                Map(m => m.Platform__c).Index(11);
                Map(m => m.Email).Index(12);
                Map(m => m.HasOptedOutOfEmail).Index(13);
                Map(m => m.HomePhone).Index(14);
                Map(m => m.Phone).Index(15);
                Map(m => m.MobilePhone).Index(16);
                Map(m => m.MailingStreet).Index(17);
                Map(m => m.MailingCity).Index(18);
                Map(m => m.MailingState).Index(19);
                Map(m => m.MailingPostalCode).Index(20);
                Map(m => m.MailingCountry).Index(21);
                Map(m => m.DoNotCall).Index(22);
                Map(m => m.cloupra__Addressee_Name__c).Index(23);
                Map(m => m.ComputershareCommunicationPreference__c).Index(24);
            }
        }

        public void CreateAccountFile (string filePath, IEnumerable<EnrichedData> data) {
            if (filePath == null) throw new ArgumentNullException("filePath");
            if (data == null) throw new ArgumentNullException("data");
            if (data.Count() == 0) throw new ArgumentException("data has no records");

            var minHolderOnDates = data
                .Where(g => g.HolderOnDate.HasValue)
                .GroupBy(g => g.GenericExternalId)
                .ToDictionary(k => k.Key, v => v.Min(e => e.HolderOnDate));

            var maxDataDate = data.Max(e => e.AsAtDate);

            var openAccounts = data
                .Where(e => e.AsAtDate == maxDataDate && e.Units > 0)
                .Select(e => e.GenericExternalId)
                .Distinct()
                .ToHashSet();

            var records = data
                .GroupBy(g => g.GenericExternalId)
                .SelectMany(g => g
                    .OrderByDescending(grp => grp.AsAtDate)
                    .ThenByDescending(grp => grp.MarketValue)
                    .Take(1)
                    .Select(grp => new PractifiAccount {
                        LastName = grp.AccountName,
                        cloupra__Business_Number__c = grp.GenericExternalId,
                        cloupra__External_Id__c = grp.AccountExternalId,
                        cloupra__Primary_Division__c = divisionId,
                        LeadSource = "Computershare",
                        cloupra__Person_Segment__c = "PSEGINVESTORACC",
                        InvestorAccountType__c = grp.AccountType.ToString(),
                        cloupra__Stage__c = openAccounts.Contains(grp.GenericExternalId) ? "PSACTIVE" : "PSINACTIVE",
                        Birthdate = minHolderOnDates.ContainsKey(grp.GenericExternalId) ? minHolderOnDates[grp.GenericExternalId] : null, // TODO: Deal with null account open date
                        BrokerName__c = grp.BrokerName,
                        IsAdvised__c = grp.HasPlanner,
                        Platform__c = grp.Platform,
                        Email = grp.Email,
                        HasOptedOutOfEmail = grp.EmailOptOut,
                        Phone = grp.WorkPhone,
                        MobilePhone = grp.MobilePhone,
                        HomePhone = grp.HomePhone,
                        DoNotCall = grp.DoNotCall,
                        MailingStreet = grp.PostalStreet,
                        MailingCity = grp.PostalCity,
                        MailingState = grp.PostalState,
                        MailingPostalCode = grp.PostalCode,
                        MailingCountry = grp.PostalCountry,
                        cloupra__Addressee_Name__c = string.Join(" & ", grp.People.Count >= 1 ? grp.People.Select(e => e.FirstName) : grp.Entities.Select(e => e.Name)),
                        ComputershareCommunicationPreference__c = grp.ContactPreference.ToString(),
                    })
                )
                .ToList();

            using (var writer = new StreamWriter(filePath))
            using (var csv = new CsvWriter(writer)) {
                csv.Configuration.RegisterClassMap<PractifiAccountMap>();
                csv.WriteRecords(records);
            }
            logger.Info("Created account file with {0} records.", records.Count());
        }

        #endregion

        #region Portfolio Holdings

        public class PractifiPortfolioHolding {
            public string cloupra__ExternalId__c { get; set; }
            public string cloupra__Person__c { get; set; }
            public string cloupra__Data_Feed__c { get; set; }
            public string cloupra__Provider__c { get; set; }
            public DateTime cloupra__Valuation_Date__c { get; set; }
            public decimal Inflows__c { get; set; }
            public decimal Outflows__c { get; set; }
            public decimal cloupra__Current_Value__c { get; set; }
            public string cloupra__Portfolio_Holding_Type__c { get; set; }
            public string cloupra__Primary_Division__c { get; set; }
            public bool cloupra__Under_Advice__c { get; set; }
        }

        public class PractifiPortfolioHoldingMap : ClassMap<PractifiPortfolioHolding> {
            public PractifiPortfolioHoldingMap() {
                Map(m => m.cloupra__ExternalId__c).Index(0);
                Map(m => m.cloupra__Person__c).Index(1).Name("cloupra__Person__r.cloupra__External_Id__c");
                Map(m => m.cloupra__Data_Feed__c).Index(2).Name("cloupra__Data_Feed__r.cloupra__Code__c");
                Map(m => m.cloupra__Provider__c).Index(3).Name("cloupra__Provider__r.cloupra__External_Id__c");
                Map(m => m.cloupra__Valuation_Date__c).Index(4).TypeConverter<SfdcDateConverter>();
                Map(m => m.Inflows__c).Index(5).TypeConverter<SfdcCurrencyConverter>();
                Map(m => m.Outflows__c).Index(6).TypeConverter<SfdcCurrencyConverter>();
                Map(m => m.cloupra__Current_Value__c).Index(7).TypeConverter<SfdcCurrencyConverter>();
                Map(m => m.cloupra__Portfolio_Holding_Type__c).Index(8).Name("cloupra__Portfolio_Holding_Type__r.cloupra__Code__c");
                Map(m => m.cloupra__Under_Advice__c).Index(9);
                Map(m => m.cloupra__Primary_Division__c).Index(10).Name("cloupra__Primary_Division__r.cloupra__External_Id__c");
            }
        }

        public void CreatePortfolioHoldingFile(string filePath, IEnumerable<EnrichedData> data) {
            if (filePath == null) throw new ArgumentNullException("filePath");
            if (data == null) throw new ArgumentNullException("data");
            if (data.Count() == 0) throw new ArgumentException("data has no records");

            var records = data
                .Select(e => new PractifiPortfolioHolding {
                    cloupra__ExternalId__c = e.AssetExternalId,
                    cloupra__Person__c = e.AccountExternalId,
                    cloupra__Data_Feed__c = datafeedCode,
                    cloupra__Provider__c = e.AsxCode,
                    cloupra__Valuation_Date__c = e.AsAtDate,
                    Inflows__c = Math.Abs(e.MarketValueBought),
                    Outflows__c = Math.Abs(e.MarketValueSold),
                    cloupra__Current_Value__c = e.MarketValue,
                    cloupra__Portfolio_Holding_Type__c = "PFLOASSET",
                    cloupra__Under_Advice__c = true,
                    cloupra__Primary_Division__c = divisionId,
                })
                .ToList();

            using (var writer = new StreamWriter(filePath))
            using (var csv = new CsvWriter(writer)) {
                csv.Configuration.RegisterClassMap<PractifiPortfolioHoldingMap>();
                csv.WriteRecords(records);
            }
            logger.Info("Created portfolio holding file with {0} records.", records.Count());
        }

        #endregion

        #region Data Feed

        public class PractifiDataFeed {
            public string cloupra__Code__c { get; set; }
            public DateTime LatestValuationDate__c { get; set; }
        }

        public class PractifiDataFeedMap : ClassMap<PractifiDataFeed> {
            public PractifiDataFeedMap() {
                Map(m => m.cloupra__Code__c).Index(0);
                Map(m => m.LatestValuationDate__c).Index(1).TypeConverter<SfdcDateConverter>();
            }
        }

        public void CreateDataFeedFile(string filePath, IEnumerable<EnrichedData> data) {
            if (filePath == null) throw new ArgumentNullException("filePath");
            if (data == null) throw new ArgumentNullException("data");
            if (data.Count() == 0) throw new ArgumentException("data has no records");
            var records = new PractifiDataFeed[] {
                new PractifiDataFeed() {
                    cloupra__Code__c = datafeedCode,
                    LatestValuationDate__c = data.Max(e => e.AsAtDate),
                }
            };
            using (var writer = new StreamWriter(filePath))
            using (var csv = new CsvWriter(writer)) {
                csv.Configuration.RegisterClassMap<PractifiDataFeedMap>();
                csv.WriteRecords(records);
            }
            logger.Info("Created data feed file.");
        }

        #endregion

    }
}
