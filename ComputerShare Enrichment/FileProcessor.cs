﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using PhoneNumbers;
using NLog;
using CsvHelper;

namespace ComputerShare_Enrichment {
    public class FileProcessor {

        #region Constructor

        private string[] SuperannuationCommonText;
        private HashSet<string> PlannerCommonText;
        private Dictionary<string, string> PlatformMap;
        private Dictionary<string, string> StateMap;

        private PhoneNumberUtil pnu;
        private TextInfo ti;
        private List<RegionInfo> Countries;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public FileProcessor () {
            PopulateRegionInfo();
            pnu = PhoneNumberUtil.GetInstance();
            ti = CultureInfo.CurrentCulture.TextInfo;
        }

        public void PopulateLists () {
            SuperannuationCommonText = new string[] { "SUPER", "S/FUND", "S/F", "SF ", "SMSF", "PENSION", "TRUSTEES" };

            StateMap = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase) {
                { "New South Wales", "NSW" },
                { "Queensland", "QLD" },
                { "Australian Capital Territory", "ACT" },
                { "Tasmania", "TAS" },
                { "South Australia", "SA" },
                { "Western Australia", "WA" },
                { "Nothern Territory", "NT" },
                { "Victoria", "VIC" },
            };

            var plannerPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), @"Maps/PlannerCommonText.txt");
            using (var reader = new StreamReader(plannerPath)) {
                PlannerCommonText = reader.ReadToEnd().Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToHashSet(StringComparer.InvariantCultureIgnoreCase);
            }
            logger.Info("Read {0} PlannerCommonText records", PlannerCommonText.Count);

            PlatformMap = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
            var platformMapPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), @"Maps/PlatformMap.csv");
            using (var reader = new StreamReader(platformMapPath))
            using (var csv = new CsvReader(reader)) {
                csv.Configuration.Delimiter = ",";
                csv.Configuration.DetectColumnCountChanges = true;
                csv.Read();
                while (csv.Read()) {
                    PlatformMap.Add(csv.GetField(0), csv.GetField(1));
                }
            }
            logger.Info("Read {0} PlatformMap records", PlatformMap.Count);
        }

        private void PopulateRegionInfo () {
            CultureInfo[] Cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            Countries = new List<RegionInfo>();
            foreach (CultureInfo ci in Cultures) {
                RegionInfo regionInfo = new RegionInfo(ci.Name);
                if (Countries.Count(x => x.EnglishName == regionInfo.EnglishName) <= 0) {
                    Countries.Add(regionInfo);
                }
            }
        }

        #endregion

        #region Read Computershare File

        /// <summary>
        /// Check headers are valid. There are slight differences in headers between listed funds
        /// which are negated using the regex in this function.
        /// </summary>
        private bool VerifyCpuFileHeader (string actual, params string[] expected) {
            if (expected == null) {
                throw new ArgumentNullException("expected");
            }
            if (expected.Length == 0) {
                throw new ArgumentException("expected == 0");
            }
            if (actual == null) {
                throw new ArgumentNullException("actual");
            }
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            actual = rgx.Replace(actual, string.Empty).Replace(" ", string.Empty);
            foreach (var e in expected) {
                string s = rgx.Replace(e, string.Empty).Replace(" ", string.Empty);
                if (s.Equals(actual, StringComparison.OrdinalIgnoreCase)) {
                    return true;
                }
            }
            return false;
        }

        public CpuDataCollection ReadCpuFile (string filePath) {
            if (filePath == null) {
                throw new ArgumentNullException("filePath");
            }
            if (!File.Exists(filePath)) {
                throw new FileNotFoundException("Computershare input file not found.", filePath);
            }

            logger.Info("Reading Computershare input file: {0}", Path.GetFileName(filePath));
            var wb = new LinqToExcel.ExcelQueryFactory(filePath);

            // verify headers are correct

            string[] headers = wb.GetColumnNames(wb.GetWorksheetNames().First(), "A1:AI1").ToArray();
            if (!VerifyCpuFileHeader(headers[0], "F1", "HOLDER_IDENTIFIER")) throw new FormatException("Did not find 'Hin' in column 1 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[1], "F2", "SURNAME_1")) throw new FormatException("Did not find 'Person1_Surname' in column 2 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[2], "F3", "FIRST_NAME_1")) throw new FormatException("Did not find 'Person1_Name1' in column 3 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[3], "F4", "FIRST_NAME_2")) throw new FormatException("Did not find 'Person2_Name1' in column 4 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[4], "F5", "FIRST_NAME_3")) throw new FormatException("Did not find 'Person3_Name1' in column 5 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[5], "F6", "COMPANY_NAME_1")) throw new FormatException("Did not find 'CompanyName1' in column 6 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[6], "F7", "SURNAME_2")) throw new FormatException("Did not find 'Person2_Surname' in column 7 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[7], "F8", "SECOND_NAME_1")) throw new FormatException("Did not find 'Person1_Name2' in column 8 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[8], "F9", "SECOND_NAME_2")) throw new FormatException("Did not find 'Person2_Name2' in column 9 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[9], "F10", "SECOND_NAME_3")) throw new FormatException("Did not find 'Person3_Name2' in column 10 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[10], "F11", "COMPANY_NAME_2")) throw new FormatException("Did not find 'CompanyName2' in column 11 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[11], "F12", "SURNAME_3")) throw new FormatException("Did not find 'Person3_Surname' in column 12 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[12], "F13", "THIRD_NAME_1")) throw new FormatException("Did not find 'Person1_Name3' in column 13 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[13], "F14", "THIRD_NAME_2")) throw new FormatException("Did not find 'Person2_Name3' in column 14 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[14], "F15", "THIRD_NAME_3")) throw new FormatException("Did not find 'Person3_Name3' in column 15 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[15], "F16", "COMPANY_NAME_3")) throw new FormatException("Did not find 'CompanyName3' in column 16 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[16], "F17", "ACCOUNT_DESIGNATION", "DESIGNATOR")) throw new FormatException("Did not find 'AccountDesignation' in column 17 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[17], "F18", "ADDRESS_LINE_1")) throw new FormatException("Did not find 'AddressLine1' in column 18 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[18], "F19", "ADDRESS_LINE_2")) throw new FormatException("Did not find 'AddressLine2' in column 19 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[19], "F20", "ADDRESS_LINE_3")) throw new FormatException("Did not find 'AddressLine3' in column 20 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[20], "F21", "ADDRESS_LINE_4")) throw new FormatException("Did not find 'AddressLine4' in column 21 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[21], "F22", "ADDRESS_LINE_5")) throw new FormatException("Did not find 'AddressLine5' in column 22 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[22], "F23", "ADDRESS_LINE_6")) throw new FormatException("Did not find 'AddressLine6' in column 23 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[23], "F24", "ADDRESS_LINE_7")) throw new FormatException("Did not find 'AddressLine7' in column 24 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[24], "F25", "POSTCODE")) throw new FormatException("Did not find 'PostalCode' in column 25 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[25], "F26", "DOMICILE")) throw new FormatException("Did not find 'Country' in column 26 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[26], "F27", "HOLDER_ON_DATE")) throw new FormatException("Did not find 'HolderOnDate' in column 27 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[27], "F28", "BROKER_CODE")) throw new FormatException("Did not find 'BrokerCode' in column 28 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[28], "F29", "BROKER_NAME", "Broker")) throw new FormatException("Did not find 'BrokerName' in column 29 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[29], "F30", "HOME_PHONE")) throw new FormatException("Did not find 'HomePhone' in column 30 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[30], "F31", "WORK_PHONE")) throw new FormatException("Did not find 'WorkPhone' in column 31 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[31], "F32", "MOBILE_PHONE")) throw new FormatException("Did not find 'MobilePhone' in column 32 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[32], "F33", "EMAIL_ADDRESS")) throw new FormatException("Did not find 'EmailAddress' in column 33 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[33], "F34", "Company Information")) throw new FormatException("Did not find 'CompanyInformation' in column 34 of: " + filePath);
            if (!VerifyCpuFileHeader(headers[34], "F35", "FP_CLASS_TOTAL", "CLASS_GROUP_1", "Total Units", "ORD_CLASS_TOTAL")) throw new FormatException("Did not find 'Units' in column 35 of: " + filePath);

            var query = from row in wb.Worksheet(0)
                        let item = new CpuData() {
                            Hin = row[0].Cast<string>(),
                            Person1_Surname = row[1].Cast<string>(),
                            Person1_Name1 = row[2].Cast<string>(),
                            Person2_Name1 = row[3].Cast<string>(),
                            Person3_Name1 = row[4].Cast<string>(),
                            CompanyName1 = row[5].Cast<string>(),
                            Person2_Surname = row[6].Cast<string>(),
                            Person1_Name2 = row[7].Cast<string>(),
                            Person2_Name2 = row[8].Cast<string>(),
                            Person3_Name2 = row[9].Cast<string>(),
                            CompanyName2 = row[10].Cast<string>(),
                            Person3_Surname = row[11].Cast<string>(),
                            Person1_Name3 = row[12].Cast<string>(),
                            Person2_Name3 = row[13].Cast<string>(),
                            Person3_Name3 = row[14].Cast<string>(),
                            CompanyName3 = row[15].Cast<string>(),
                            AccountDesignation = row[16].Cast<string>(),
                            AddressLine1 = row[17].Cast<string>(),
                            AddressLine2 = row[18].Cast<string>(),
                            AddressLine3 = row[19].Cast<string>(),
                            AddressLine4 = row[20].Cast<string>(),
                            AddressLine5 = row[21].Cast<string>(),
                            AddressLine6 = row[22].Cast<string>(),
                            AddressLine7 = row[23].Cast<string>(),
                            PostalCode = row[24].Cast<string>(),
                            Country = row[25].Cast<string>(),
                            HolderOnDate = row[26].Cast<DateTime>(),
                            BrokerCode = row[27].Cast<string>(),
                            BrokerName = row[28].Cast<string>(),
                            HomePhone = row[29].Cast<string>(),
                            WorkPhone = row[30].Cast<string>(),
                            MobilePhone = row[31].Cast<string>(),
                            Email = row[32].Cast<string>(),
                            ContactPreference = row[33].Cast<string>(),
                            Units = row[34].Cast<decimal>(),
                        }
                        select item;

            var collection = new CpuDataCollection();
            foreach (CpuData record in query) {
                logger.Trace("Read HIN [{0}]");
                collection.Add(record);
            }
            if (collection.Count > 0) {
                logger.Info("Finished reading {0} rows", collection.Count);
            } else {
                logger.Warn("Finished reading {0} rows", collection.Count);
            }
            return collection;
        }

        #endregion

        #region Enrich Computershare Data

        public IEnumerable<EnrichedData> EnrichCpuData (CpuDataCollection curr, CpuDataCollection prev, DateTime asAtDate, string asxCode, decimal price, decimal vwap) {

            if (curr == null) throw new ArgumentNullException("curr");
            if (prev == null) throw new ArgumentNullException("prev");
            if (asxCode == null) throw new ArgumentNullException("asxCode");
            if (asAtDate == default) throw new ArgumentException("Invalid asAtDate");
            if (price == default || price <= 0m) throw new ArgumentException("Invalid price");
            if (vwap == default || vwap <= 0m) throw new ArgumentException("Invalid vwap");
            if (curr.Count == 0) throw new ArgumentException("Current month's CpuData list has 0 records");
            if (prev.Count == 0) throw new ArgumentException("Previous month's CpuData list has 0 records");

            logger.Info("Started enriching dataset");

            // Add in zero balances rows to the current data collection
            int counter = 0;
            foreach (var record in prev) {
                if (!curr.Contains(record.Hin)) {
                    var swap = (CpuData)record.Clone();
                    swap.Units = 0;
                    curr.Add(swap);
                    counter++;
                }
            }
            logger.Info("Inserted {0} zero balance records into the current dataset", counter);

            // Enrich the data
            int newCounter = 0;
            counter = 0;
            foreach (var record in curr) {

                var e = new EnrichedData();
                e.AsAtDate = asAtDate;
                e.AsxCode = asxCode.ToUpper();
                e.Hin = record.Hin;
                logger.Trace("Enriched data for {0} {1} {2}", e.AsAtDate.ToString("yyyy-MM-dd"), e.AsxCode, e.Hin);

                e.People = CreatePeople(record, e.GenericExternalId).ToList();
                e.Entities = CreateEntities(record, e.GenericExternalId).ToList();

                var c = new List<Client>();
                c.AddRange(e.People);
                c.AddRange(e.Entities);
                e.ClientName = string.Join(" & ", c.OrderBy(i => i.Sequence).Select(s => s.Name));
                e.AccountName = e.ClientName + (record.AccountDesignation == null ? string.Empty : " <" + record.AccountDesignation + ">");

                e.CombinedPostalAddress = string.Join(Environment.NewLine, 
                    new string[] { record.AddressLine1, record.AddressLine2, record.AddressLine3, record.AddressLine4, record.AddressLine5, record.AddressLine6, record.AddressLine7 }
                    .Where(s => !string.IsNullOrEmpty(s))
                    .Select(s => s.Trim())
                );

                e.PostalCountry = record.Country ?? "AUS";
                RegionInfo ri = Countries.Where(x => x.ThreeLetterISORegionName.Equals(e.PostalCountry, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (ri != null) {
                    e.PostalCountry = ri.EnglishName;
                }

                var addressComponents = new List<string>();
                if (!string.IsNullOrWhiteSpace(record.AddressLine7)) addressComponents.Add(record.AddressLine7.Trim());
                if (!string.IsNullOrWhiteSpace(record.AddressLine6)) addressComponents.Add(record.AddressLine6.Trim());
                if (!string.IsNullOrWhiteSpace(record.AddressLine5)) addressComponents.Add(record.AddressLine5.Trim());
                if (!string.IsNullOrWhiteSpace(record.AddressLine4)) addressComponents.Add(record.AddressLine4.Trim());
                if (!string.IsNullOrWhiteSpace(record.AddressLine3)) addressComponents.Add(record.AddressLine3.Trim());
                if (!string.IsNullOrWhiteSpace(record.AddressLine2)) addressComponents.Add(record.AddressLine2.Trim());
                if (!string.IsNullOrWhiteSpace(record.AddressLine1)) addressComponents.Add(record.AddressLine1.Trim());
                e.PostalStreet = string.Empty;
                e.PostalCode = record.PostalCode;
                bool addressComponentsFound = false;
                foreach (var addressComponent in addressComponents) {
                    if (!addressComponentsFound && !string.IsNullOrWhiteSpace(e.PostalCode) && addressComponent.IndexOf(e.PostalCode, StringComparison.OrdinalIgnoreCase) > 0) {
                        string[] split = addressComponent.Split(' ');
                        if (split.Length >= 3) {
                            addressComponentsFound = true;
                            e.PostalStreet = string.Empty;
                            e.PostalState = split[split.Length - 2];
                            e.PostalState = (e.PostalState != null && StateMap.ContainsKey(e.PostalState)) ? StateMap[e.PostalState] : e.PostalState;
                            e.PostalCity = ti.ToTitleCase(string.Join(" ", split.Take(split.Length - 2)).ToLower());
                        }
                    } else if (!e.PostalCountry.StartsWith(addressComponent, StringComparison.OrdinalIgnoreCase)) {
                        e.PostalStreet = e.PostalStreet.Insert(0, addressComponent + Environment.NewLine);
                    }
                }
                e.PostalStreet = ti.ToTitleCase(e.PostalStreet.Trim().TrimEnd('\r', '\n').ToLower());
                
                e.HolderOnDate = record.HolderOnDate != default(DateTime) ? record.HolderOnDate : (DateTime?)null;

                e.BrokerCode = record.BrokerCode;
                e.BrokerName = record.BrokerName;

                PhoneNumber pn;
                if (record.HomePhone != null) {
                    pn = StandardisePhoneNumber(record.HomePhone, e.PostalCountry);
                    if (pn != null) {
                        e.HomePhone = pnu.Format(pn, PhoneNumberFormat.INTERNATIONAL);
                    }
                }
                if (record.WorkPhone != null) {
                    pn = StandardisePhoneNumber(record.WorkPhone, e.PostalCountry);
                    if (pn != null) {
                        e.WorkPhone = pnu.Format(pn, PhoneNumberFormat.INTERNATIONAL);
                    }
                }
                if (record.MobilePhone != null) {
                    pn = StandardisePhoneNumber(record.MobilePhone, e.PostalCountry);
                    if (pn != null) {
                        e.MobilePhone = pnu.Format(pn, PhoneNumberFormat.INTERNATIONAL);
                    }
                }

                e.Email = record.Email.IsValidEmail() ? record.Email.ToLower() : null;
                switch (record.ContactPreference) {
                    case "":
                    case null:
                        break;
                    case "E":
                        e.ContactPreference = ContactPreference.Email;
                        break;
                    case "N":
                        e.ContactPreference = ContactPreference.DoNotContact;
                        break;
                    case "O":
                        e.ContactPreference = ContactPreference.Post;
                        break;
                    default:
                        logger.Debug("Unrecognised contact preference: {0}", record.ContactPreference);
                        break;
                }

                e.Units = record.Units;
                e.Price = price;

                if (prev.Contains(e.Hin)) {
                    e.UnitsTraded = e.Units - prev[e.Hin].Units;
                } else {
                    e.UnitsTraded = e.Units;
                    newCounter++;
                }
                e.Vwap = vwap;

                foreach (var pair in PlatformMap) {
                    if (e.AccountName.IndexOf(pair.Key, StringComparison.OrdinalIgnoreCase) >= 0 || e.CombinedPostalAddress.IndexOf(pair.Key, StringComparison.OrdinalIgnoreCase) >= 0) {
                        e.Platform = pair.Value;
                        break;
                    }
                }

                e.AccountType = e.Entities.Count > 0 ? AccountType.Company : AccountType.Individual; // default account type
                if (SuperannuationCommonText.Any(s => e.AccountName.IndexOf(s, StringComparison.OrdinalIgnoreCase) >= 0)) {
                    e.AccountType = AccountType.Superfund;
                } else if (e.Platform != null) {
                    e.AccountType = AccountType.Platform;
                }

                e.HasPlanner = PlannerCommonText.Any(s => e.AccountName.IndexOf(s, StringComparison.OrdinalIgnoreCase) >= 0) || PlannerCommonText.Any(s => e.CombinedPostalAddress.IndexOf(s, StringComparison.OrdinalIgnoreCase) >= 0);

                yield return e;

                counter++;
            }
            logger.Info("Detected {0} new balances in the current dataset", newCounter);
            logger.Info("Enriched {0} records in the current dataset", counter);
        }

        private IEnumerable<Person> CreatePeople (CpuData record, string externalId) {
            if (record == null) {
                throw new ArgumentNullException("record");
            }
            if (externalId == null) {
                throw new ArgumentNullException("accountExternalId");
            }
            if (record.Person1_Name1 != null || record.Person1_Name2 != null || record.Person1_Name3 != null || record.Person1_Surname != null) {
                var e = new Person();
                e.Sequence = 1;
                e.ExternalId = externalId;
                e.FirstName = record.Person1_Name1?.Trim();
                e.MiddleName = string.Join(" ", new string[] { record.Person1_Name2, record.Person1_Name3 }.Where(s => !string.IsNullOrEmpty(s)).Select(s => s.Trim()));
                e.LastName = record.Person1_Surname?.Trim();
                yield return e;
            }
            if (record.Person2_Name1 != null || record.Person2_Name2 != null || record.Person2_Name3 != null || record.Person2_Surname != null) {
                var e = new Person();
                e.Sequence = 2;
                e.ExternalId = externalId;
                e.FirstName = record.Person2_Name1?.Trim();
                e.MiddleName = string.Join(" ", new string[] { record.Person2_Name2, record.Person2_Name3 }.Where(s => !string.IsNullOrEmpty(s)).Select(s => s.Trim()));
                e.LastName = record.Person2_Surname?.Trim();
                yield return e;
            }
            if (record.Person3_Name1 != null || record.Person3_Name2 != null || record.Person3_Name3 != null || record.Person3_Surname != null) {
                var e = new Person();
                e.Sequence = 3;
                e.ExternalId = externalId;
                e.FirstName = record.Person3_Name1?.Trim();
                e.MiddleName = string.Join(" ", new string[] { record.Person3_Name2, record.Person3_Name3 }.Where(s => !string.IsNullOrEmpty(s)).Select(s => s.Trim()));
                e.LastName = record.Person3_Surname?.Trim();
                yield return e;
            }
        }

        private IEnumerable<Entity> CreateEntities(CpuData record, string externalId) {
            if (record == null) {
                throw new ArgumentNullException("record");
            }
            if (externalId == null) {
                throw new ArgumentNullException("accountExternalId");
            }
            if (record.CompanyName1 != null) {
                var e = new Entity();
                e.Sequence = 1;
                e.ExternalId = externalId;
                e.Name = record.CompanyName1?.Trim();
                yield return e;
            }
            if (record.CompanyName2 != null) {
                var e = new Entity();
                e.Sequence = 2;
                e.ExternalId = externalId;
                e.Name = record.CompanyName2?.Trim();
                yield return e;
            }
            if (record.CompanyName3 != null) {
                var e = new Entity();
                e.Sequence = 3;
                e.ExternalId = externalId;
                e.Name = record.CompanyName3?.Trim();
                yield return e;
            }
        }

        #endregion

        #region Phone Number Parsers

        private PhoneNumber StandardisePhoneNumber (string numberString, string region = null) {
            if (numberString == null) {
                return null;
            }
            PhoneNumber pn = null;
            if (numberString.StartsWith("+")) { // most of the DB will already be in intl format already after the initial clean
                pn = GeneratePhoneNumber(numberString, "ZZ");
            }
            if (pn == null && region != null) {
                string isoCode = ConvertStringToTwoLetterISORegionName(region); // then try the contact's region
                if (isoCode != null) {
                    pn = GeneratePhoneNumber(numberString, isoCode);
                }
            }
            if (pn == null) {
                pn = GeneratePhoneNumber(numberString, "AU"); // otherwise try Australia then New Zealand
            }
            if (pn == null && region == null) {
                pn = GeneratePhoneNumber(numberString, "NZ");
            }
            if (pn == null) {
                pn = GeneratePhoneNumber("+" + numberString, "ZZ");
            }
            return pn;
        }

        private string ConvertStringToTwoLetterISORegionName (string region) {
            if (region == null) {
                throw new ArgumentNullException("region");
            }
            RegionInfo ri;
            // Equality
            ri = Countries.Where(e => e.EnglishName.Equals(region, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (ri != null) {
                return ri.TwoLetterISORegionName;
            }
            ri = Countries.Where(e => e.DisplayName.Equals(region, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (ri != null) {
                return ri.TwoLetterISORegionName;
            }
            ri = Countries.Where(e => e.NativeName.Equals(region, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (ri != null) {
                return ri.TwoLetterISORegionName;
            }
            ri = Countries.Where(e => e.ThreeLetterISORegionName.Equals(region, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (ri != null) {
                return ri.TwoLetterISORegionName;
            }
            ri = Countries.Where(e => e.TwoLetterISORegionName.Equals(region, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (ri != null) {
                return ri.TwoLetterISORegionName;
            }
            // Region starts with input
            ri = Countries.Where(e => e.EnglishName.StartsWith(region, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (ri != null) {
                return ri.TwoLetterISORegionName;
            }
            ri = Countries.Where(e => e.DisplayName.StartsWith(region, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (ri != null) {
                return ri.TwoLetterISORegionName;
            }
            ri = Countries.Where(e => e.NativeName.StartsWith(region, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (ri != null) {
                return ri.TwoLetterISORegionName;
            }
            // Input starts with region
            ri = Countries.Where(e => region.StartsWith(e.EnglishName, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (ri != null) {
                return ri.TwoLetterISORegionName;
            }
            ri = Countries.Where(e => region.StartsWith(e.DisplayName, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (ri != null) {
                return ri.TwoLetterISORegionName;
            }
            ri = Countries.Where(e => region.StartsWith(e.NativeName, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (ri != null) {
                return ri.TwoLetterISORegionName;
            }
            return null;
        }

        private PhoneNumber GeneratePhoneNumber (string numberString, string defaultRegion) {
            if (numberString == null) {
                throw new ArgumentNullException("numberString");
            }
            if (defaultRegion == null) {
                throw new ArgumentNullException("region");
            }
            PhoneNumber numberProto;
            try {
                numberProto = pnu.Parse(numberString, defaultRegion);
            } catch (NumberParseException) {
                return null;
            }
            if (pnu.IsValidNumber(numberProto)) {
                return numberProto;
            } else {
                return null;
            }
        }

        #endregion

    }
}
