﻿namespace ComputerShare_Enrichment {
    public enum ContactPreference {
        Email,
        DoNotContact,
        Post
    }
}
