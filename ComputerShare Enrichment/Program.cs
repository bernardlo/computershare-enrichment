﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using CsvHelper;
using NLog;

namespace ComputerShare_Enrichment {

    class Program {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        static void Main (string[] args) {

            string onedriveRelativePath = ConfigurationManager.AppSettings["OnedriveRelativePath"];
            string sfdcUserName = ConfigurationManager.AppSettings["SfdcUserName"];
            string sfdcPassword = ConfigurationManager.AppSettings["SfdcPassword"];
            string sfdcToken = ConfigurationManager.AppSettings["SfdcToken"];

#if !DEBUG
            try {
#endif

                logger.Info("Starting program.");

                if (string.IsNullOrWhiteSpace(onedriveRelativePath)) {
                    throw new ArgumentNullException("onedriveRelativePath");
                }

                var onedriveDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "Pengana Holdings Pty Ltd");
                if (!Directory.Exists(onedriveDir)) {
                    throw new DirectoryNotFoundException("Could not find commercial OneDrive folder: " + onedriveDir);
                }
                if (onedriveDir.IndexOf("Pengana Holdings Pty Ltd", StringComparison.OrdinalIgnoreCase) < 0) {
                    throw new InvalidDataException("OneDrive folder located is not for Pengana Holdings: " + onedriveDir);
                }

                var sourceDir = Path.Combine(onedriveDir, onedriveRelativePath);
                if (!Directory.Exists(sourceDir)) {
                    throw new DirectoryNotFoundException("Could not find source folder: " + sourceDir);
                }

                var fileSettingsFile = "./FileSettings.csv";
                if (!File.Exists(fileSettingsFile)) {
                    throw new FileNotFoundException("Missing settings file: " + fileSettingsFile);
                }
                FileSetting[] fileSettings;
                using (var reader = new StreamReader(fileSettingsFile))
                using (var csv = new CsvReader(reader)) {
                    csv.Configuration.RegisterClassMap<FileSettingMap>();
                    fileSettings = csv.GetRecords<FileSetting>().ToArray();
                }

                var fp = new FileProcessor();
                fp.PopulateLists();
                var records = new List<EnrichedData>();
                foreach (var fs in fileSettings) {
                    var currData = fp.ReadCpuFile(Path.Combine(sourceDir, fs.CurrFilePath));
                    var prevData = fp.ReadCpuFile(Path.Combine(sourceDir, fs.PrevFilePath));
                    records.AddRange(fp.EnrichCpuData(currData, prevData, fs.AsAtDate, fs.AsxCode, fs.Price, fs.Vwap));
                }
                logger.Info("Completed file processing and enrichment without errors.");

                var outputPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Output");
                Directory.CreateDirectory(outputPath);

                var o = new UploadFileCreator();
                o.CreateEnrichedDataFile(Path.Combine(outputPath, "Report_Complete.csv"), records);
                o.CreateAccountFile(Path.Combine(outputPath, "PFI_DataLoader_Contact.csv"), records);
                o.CreatePortfolioHoldingFile(Path.Combine(outputPath, "PFI_DataLoader_PortfolioHolding.csv"), records);
                o.CreateDataFeedFile(Path.Combine(outputPath, "PFI_DataLoader_DataFeed.csv"), records);
                logger.Info("Created upload files without errors.");

                var summary = PrintSummary(records);
                logger.Info(summary);
                Console.WriteLine("\n\n" + summary + "\nReview the summary above and/or the output files for any obvious issues.");
                while (true) {
                    Console.WriteLine("Do you want to continue with loading data into Practifi (Y/N)?");
                    string s = Console.ReadLine();
                    if (s.Equals("Y", StringComparison.OrdinalIgnoreCase)) {
                        break;
                    } else if (s.Equals("N", StringComparison.OrdinalIgnoreCase)) {
                        return;
                    }
                }

                if (!string.IsNullOrWhiteSpace(sfdcUserName) && !string.IsNullOrWhiteSpace(sfdcPassword)) {
                    logger.Info("Preparing to upsert records.");
                    using (var dataUpsert = new DataUpsert(sfdcUserName, sfdcPassword, sfdcToken)) {
                        dataUpsert.UpsertContact(records);
                        dataUpsert.UpsertPortfolioHolding(records);
                        dataUpsert.UpsertDataFeed(records);
                    }
                    logger.Info("Upserted records without errors.");
                } else {
                    logger.Info("Skipping upsert.");
                }

#if !DEBUG
            } catch (Exception e) {
                logger.Error(e, "Error - program was terminated.");
            }
#endif

        }

        static string PrintSummary(IEnumerable<EnrichedData> data) {
            if (data == null) {
                throw new ArgumentNullException("data");
            }
            var sb = new StringBuilder();
            foreach (var g in data.GroupBy(e => new Tuple<DateTime, string>(e.AsAtDate, e.AsxCode)).OrderBy(e => e.Key.Item1).ThenBy(e => e.Key.Item2)) {
                sb.AppendLine().Append("  ***  ").Append(g.Key.Item1.ToString("yyyy-MM-dd")).Append(" - ").Append(g.Key.Item2).AppendLine("  ***").AppendLine();
                sb.Append("    Count (All)         = ").AppendLine(string.Format("{0:n0}", g.Count()));
                sb.Append("    Count (New)         = ").AppendLine(string.Format("{0:n0}", g.Where(e => e.HoldingType == HoldingType.New).Count()));
                sb.Append("    Count (Existing)    = ").AppendLine(string.Format("{0:n0}", g.Where(e => e.HoldingType == HoldingType.Existing).Count()));
                sb.Append("    Count (Closed Out)  = ").AppendLine(string.Format("{0:n0}", g.Where(e => e.HoldingType == HoldingType.Closed).Count()));
                sb.Append("    Units on Issue      = ").AppendLine(string.Format("{0:n0}", g.Sum(e => e.Units)));
                sb.Append("    Units Changed       = ").AppendLine(string.Format("{0:n0}", g.Sum(e => e.UnitsTraded)));
                sb.Append("    Market Value        = ").AppendLine(string.Format("{0:n0}", g.Sum(e => e.MarketValue)));
                sb.Append("    Market Value Bought = ").AppendLine(string.Format("{0:n0}", g.Sum(e => e.MarketValueBought)));
                sb.Append("    Market Value Sold   = ").AppendLine(string.Format("{0:n0}", g.Sum(e => e.MarketValueSold)));
                sb.Append("    Market Value Traded = ").AppendLine(string.Format("{0:n0}", g.Sum(e => e.MarketValueTraded)));
                sb.AppendLine();
            }
            return sb.ToString();
        }

    }
}
