﻿namespace ComputerShare_Enrichment {
    public enum HoldingType {
        New = 0,
        Existing = 1,
        Closed = 2
    }
}
