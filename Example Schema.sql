CREATE TABLE Account (
      Hin VARCHAR NOT NULL -- The Holder Identification Number or the Stock Reference Number. This is the account number.
    , AccountName VARCHAR NOT NULL -- The AccountName which is a concatenation of clients/entities and any account designation
    
    , CombinedPostalAddress VARCHAR -- A concatenation (by new line) of each of CPU's address components which are randomly split across multiple fields. Loaded into Practifi as the postal street address which can then be subsequently loaded and parsed through the Google Maps API to split it out into SFDC components.
    , PostalCode VARCHAR
    , PostalCountry VARCHAR
    , Email VARCHAR

    /*  Not quite sure how to deal with contact preferences since they're at the { Account, Product } level
     */
    , EmailOptOut BIT -- Whether this { HIN, AsxCode } combination has opted out of emails
    , ContactPreference VARCHAR -- Email, phone or postage or null (no preference) for the { HIN, AsxCode } combination

    , HomePhone VARCHAR -- Home phone number (of the account) in international format: +61 2 8524 9900. Can also contain a mobile number.
    , WorkPhone VARCHAR -- Work phone number (of the account) in international format: +61 2 8524 9900. Can also contain a mobile number.
    , MobilePhone VARCHAR -- Mobile phone number (of the account) in international format: +61 403 123 456.

    , AccountType VARCHAR -- An approximated account type based on certain strings of text appearing in the AccountName. We would want to use this field if Practifi has it as null. However, we don't want to overwrite any subsequent user input as this field is an approximation only.
    , HasPlanner BIT -- Whether the account has an adviser. This is the extent of our current analysis in Excel. Once the data is in Practifi we would attempt to join to actual advisers in the DB.

    , CONSTRAINT PK_Account PRIMARY KEY (Hin)
)
GO

/*  For the below ClientPerson and Client Entity, note that this would return a unique set of rows:
 *    SELECT Hin, Sequence FROM ClientPerson
 *    UNION ALL
 *    SELECT Hin, Sequence FROM ClientEntity
 */

CREATE TABLE ClientPerson (
      Hin VARCHAR NOT NULL -- The Holder Identification Number or the Stock Reference Number. This is the account number.
    , Sequence INT NOT NULL -- An account can have up to 3 people/entities. The sequence represents the order of appearance and is consistent over time.
    , FirstName VARCHAR
    , MiddleName VARCHAR
    , LastName VARCHAR
    , CONSTRAINT PK_ClientPerson PRIMARY KEY (Hin, Sequence)
    , CONSTRAINT FK_ClientPerson_Account FOREIGN KEY (Hin) REFERENCES Account (Hin)
)
GO

CREATE TABLE ClientEntity (
      Hin VARCHAR NOT NULL -- The Holder Identification Number or the Stock Reference Number. This is the account number.
    , Sequence INT NOT NULL -- An account can have up to 3 people/entities. The sequence represents the order of appearance and is consistent over time.
    , Name VARCHAR -- Computershare calls this a company name
    , CONSTRAINT PK_ClientEntity PRIMARY KEY (Hin, Sequence)
    , CONSTRAINT FK_ClientEntity_Account FOREIGN KEY (Hin) REFERENCES Account (Hin)
)
GO

CREATE TABLE Broker (
      BrokerCode VARCHAR NOT NULL -- Computershare's internal broker code. This is the last broker the HIN used to trade in the AsxCode. If you stored it in the assets and cashflows object then you'd see the history. Alternatively, if we stored it under the account, we would just have access to the most recently used broker. I think you can treat a broker like a platform. You will be keeping platforms in your Service object?
    , BrokerName VARCHAR
    , CONSTRAINT PK_Broker PRIMARY KEY (BrokerCode)
)
GO

CREATE TABLE AssetsAndCashflows (

      Hin VARCHAR NOT NULL -- The Holder Identification Number or the Stock Reference Number. This is the account number.
    , AsAtDate DATE NOT NULL -- Date of the price and units for assets. For trades refers to the entire month. The Date will always be a month end e.g. 31/12/2019
    , AsxCode CHAR(3) NOT NULL -- ASX Ticker. Will our fund names be stored in a separate object - if so assume there will be another FKey?
    
    , BrokerCode VARCHAR NOT NULL -- Computershare's internal broker code. This is the last broker the HIN used to trade in the AsxCode. If you stored it in the assets and cashflows object then you'd see the history. Alternatively, if we stored it under the account, we would just have access to the most recently used broker. I think you can treat a broker like a platform. You will be keeping platforms in your Service object?

    , HolderOnDate DATE -- The date the {HIN, BrokerCode} first became a holder in the stock {AsxCode}

    , Units DECIMAL(18,5) -- Number of units or shares held
    , Price DECIMAL(18,5) -- The price of each unit or share on the AsAtDate
    , MarketValue DECIMAL(18,5) -- The dollar value held in the stock

    /*  In our salesforce, we have fields for ins, outs, and nets. Not sure about Practifi?
     *  Also note that for the initial month, units traded will be zero as we won't know the difference (as described below). We are proposing that the initial month be 31 August 19.
     */

    , UnitsTraded DECIMAL(18,5) -- The units traded. This will be approximated by looking at the difference in unit balances between two ComputerShare files. I propose doing this by extracting latest data in Practifi when processing a new file from Computershare and doing the calculation prior to data import. Let me know if there's a better way.
    , UnitsBought DECIMAL(18,5) -- If UnitsTraded is positive then return it, else null.
    , UnitsSold DECIMAL(18,5) -- If UnitsTraded is negative then return it, else null.
    , Vwap DECIMAL(18,5) -- A user supplied Volume Weighted Average Price used to approximate the dollar value traded over the AsAtDate's month. 
    , MarketValueTraded DECIMAL(18,5) -- The approximate dollar value traded over the AsAtDate's month.
    , MarketValueBought DECIMAL(18,5) -- If MarketValueTraded is positive then return it, else null.
    , MarketValueSold DECIMAL(18,5) -- If MarketValueTraded is negative then return it, else null.

    , CONSTRAINT PK_AssetsAndCashflows PRIMARY KEY (Hin, AsAtDate, AsxCode)

    , CONSTRAINT FK_AssetsAndCashflows_Account FOREIGN KEY (Hin) REFERENCES Account (Hin)
    , CONSTRAINT FK_AssetsAndCashflows_Broker FOREIGN KEY (BrokerCode) REFERENCES Broker (BrokerCode)

)
GO