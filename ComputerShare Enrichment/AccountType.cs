﻿namespace ComputerShare_Enrichment {
    public enum AccountType {
        Company,
        Individual,
        Platform,
        Superfund
    }
}
