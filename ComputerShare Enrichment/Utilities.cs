﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

public static class Utilities {

    /// <summary>
    /// Convert an object to type T
    /// </summary>
    /// <typeparam name="T">The type to convert to</typeparam>
    /// <param name="obj">Object to convert</param>
    /// <returns>An object converted to type T</returns>
    public static T ConvertObject<T>(this object obj) {
        if (obj is T) {
            return (T)obj;
        } else {
            try {
                return (T)Convert.ChangeType(obj, typeof(T));
            } catch (InvalidCastException) {
                return default(T);
            }
        }
    }

    /// <remarks>https://docs.microsoft.com/en-us/dotnet/standard/base-types/how-to-verify-that-strings-are-in-valid-email-format</remarks>
    public static bool IsValidEmail(this string email) {
        if (string.IsNullOrWhiteSpace(email))
            return false;

        try {
            // Normalize the domain
            email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                  RegexOptions.None, TimeSpan.FromMilliseconds(200));

            // Examines the domain part of the email and normalizes it.
            string DomainMapper(Match match) {
                // Use IdnMapping class to convert Unicode domain names.
                var idn = new IdnMapping();

                // Pull out and process domain name (throws ArgumentException on invalid)
                var domainName = idn.GetAscii(match.Groups[2].Value);

                return match.Groups[1].Value + domainName;
            }
        } catch (RegexMatchTimeoutException) {
            return false;
        } catch (ArgumentException) {
            return false;
        }

        try {
            return Regex.IsMatch(email,
                @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
        } catch (RegexMatchTimeoutException) {
            return false;
        }
    }

    /// <summary>
    /// Convert data in a DataReader to an enumerable
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="reader"></param>
    /// <param name="projection"></param>
    /// <returns></returns>
    public static IEnumerable<T> Select<T>(this IDataReader reader, Func<IDataReader, T> projection) {
        while (reader.Read()) {
            yield return projection(reader);
        }
    }

    #region Security ID Verification Extensions

    /// <summary>
    /// For a SEDOL+CheckDigit of length 7, verify that the check digit is correct.
    /// </summary>
    /// <param name="sedol7">SEDOL+CheckDigit</param>
    public static bool Sedol7Validator(this string sedol7) {
        if (sedol7 == null) {
            throw new ArgumentNullException("sedol7");
        }
        if (sedol7.Length != 7) {
            throw new FormatException("sedol7 length is expected to equal 7");
        }
        string sedol6 = sedol7.Substring(0, 6);
        int checkDigit = Sedol6CheckDigit(sedol6);
        return sedol7 == string.Format("{0}{1}", sedol6, checkDigit);
    }

    /// <summary>
    /// For a SEDOL without a check digit, compute the check digit.
    /// </summary>
    /// <param name="sedol6">SEDOL without a check digit</param>
    /// <returns>SEDOL check digit</returns>
    public static int Sedol6CheckDigit(this string sedol6) {
        if (sedol6 == null) {
            throw new ArgumentNullException("sedol6");
        }
        sedol6 = sedol6.Trim();
        if (sedol6.Length != 6) {
            throw new FormatException("sedol6 length is expected to equal 7");
        }
        int[] sedol_weights = { 1, 3, 1, 7, 3, 9 };
        int len = sedol6.Length;
        int sum = 0;
        if (Regex.IsMatch(sedol6, "[AEIOUaeiou]+")) {
            throw new FormatException("sedol6 contained invalid characters");
        }
        for (int i = 0; i < 6; i++) {
            if (char.IsDigit(sedol6[i])) {
                sum += (((int)sedol6[i] - 48) * sedol_weights[i]);
            } else if (char.IsLetter(sedol6[i])) {
                sum += (((int)char.ToUpper(sedol6[i]) - 55) * sedol_weights[i]);
            } else {
                throw new Exception();
            }
        }
        return (10 - (sum % 10)) % 10;
    }

    /// <summary>
    /// Verify the 9th digit of a SEDOL is valid.
    /// </summary>
    /// <param name="cusip">9 digit CUSIP</param>
    public static bool CusipValidator(this string cusip) {
        if (cusip == null) {
            throw new ArgumentNullException("cusip");
        }
        if (cusip.Length != 9) {
            throw new FormatException("cusip length is not 9");
        }
        char[] cusipChars = cusip.ToCharArray();
        int sum = 0;
        for (int i = 0; i < 8; i++) {
            int num = CusipMapChar(cusipChars[i]);
            if (i % 2 != 0) { // Double all the odd digits
                num *= 2;
            }
            if (num > 9) { // Combine digits.  i.e., 16 = (1 + 6) = 7
                num = (num % 10) + (num / 10);
            }
            sum += num;
        }
        int checkDigit = CusipMapChar(cusipChars[8]);
        int mod = (10 - (sum % 10)) % 10; // This is the mathmatical modulus - not the remainder.  i.e., 10 mod 7 = 3
        return mod == checkDigit;
    }

    private static int CusipMapChar(char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        } else {
            return c - 'A' + 10;
        }
    }

    #endregion

    #region String Extensions

    public static IEnumerable<int> AllIndexesOf(this string str, string value) {
        if (String.IsNullOrEmpty(value)) {
            throw new ArgumentException("the string to find may not be empty", "value");
        }
        List<int> indexes = new List<int>();
        for (int index = 0; ; index += value.Length) {
            index = str.IndexOf(value, index, StringComparison.OrdinalIgnoreCase);
            if (index == -1) {
                break;
            }
            yield return index;
        }
    }

    /// <summary>
    /// Repeat a string
    /// </summary>
    /// <param name="value">Text to repeat</param>
    /// <param name="count">Number of times to repeat</param>
    public static string Repeat(this string value, int count) {
        return new StringBuilder(value.Length * count).Insert(0, value, count).ToString();
    }

    /// <summary>
    /// Use IndexOf StringComparison.OrdinalIgnoreCase to check whether the input string contains another string.
    /// </summary>
    /// <param name="s">input string</param>
    /// <param name="value">value to search for within input string</param>
    /// <returns></returns>
    public static bool ContainsOrdinalIgnoreCase(this string s, string value) {
        if (s.IndexOf(value, StringComparison.OrdinalIgnoreCase) == -1) {
            return false;
        } else {
            return true;
        }
    }

    /// <summary>
    /// Use IndexOf StringComparison.OrdinalIgnoreCase to check whether the input string contains another string.
    /// </summary>
    /// <param name="s">input string</param>
    /// <param name="value">value to search for within input string</param>
    /// <returns></returns>
    public static bool EqualsOrdinalIgnoreCase(this string s, string value) {
        if (s.Equals(value, StringComparison.OrdinalIgnoreCase)) {
            return true;
        } else {
            return false;
        }
    }

    #endregion

    #region Enumerable Extension Methods

    /// <summary>
    /// Wraps this object instance into an IEnumerable&lt;T&gt;
    /// consisting of a single item.
    /// </summary>
    /// <typeparam name="T"> Type of the object. </typeparam>
    /// <param name="item"> The instance that will be wrapped. </param>
    /// <returns> An IEnumerable&lt;T&gt; consisting of a single item. </returns>
    public static IEnumerable<T> Yield<T>(this T item) {
        yield return item;
    }

    /// <summary>
    /// Creates a DataTable&lt;T> from an IEnumerable&lt;T>
    /// </summary>
    /// <typeparam name="T">The type of the elements of collection.</typeparam>
    /// <param name="source">The IEnumerable&lt;T> to create a HashSet&lt;T> from.</param>
    /// <returns>A DataTable&lt;T> that contains elements from the input sequence.</returns>
    public static DataTable ToDataTable<T>(this IEnumerable<T> source) {
        PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
        DataTable table = new DataTable();
        foreach (PropertyDescriptor prop in properties) {
            table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        }
        foreach (T item in source) {
            DataRow row = table.NewRow();
            foreach (PropertyDescriptor prop in properties) {
                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
            }
            table.Rows.Add(row);
        }
        return table;
    }

    /// <summary>
    /// Creates a HashSet&lt;T> from an IEnumerable&lt;T>
    /// </summary>
    /// <typeparam name="T">The type of the elements of collection.</typeparam>
    /// <param name="collection">The collection whose elements are copied to the new set.</param>
    /// <returns>A HashSet&lt;T> that contains elements from the input sequence.</returns>
    public static HashSet<T> ToHashSet<T>(this IEnumerable<T> collection) {
        return new HashSet<T>(collection);
    }

    /// <summary>
    /// Creates a HashSet&lt;T> from an IEnumerable&lt;T>
    /// </summary>
    /// <typeparam name="T">The type of the elements of collection.</typeparam>
    /// <param name="collection">The collection whose elements are copied to the new set.</param>
    /// <param name="comparer">The IEqualityComparer&lt;T> implementation to use when comparing values in the set, or null to use the default EqualityComparer&lt;T> implementation for the set type.</param>
    /// <returns>A HashSet&lt;T> that contains elements from the input sequence that uses the specified equality comparer for the set type.</returns>
    public static HashSet<T> ToHashSet<T>(this IEnumerable<T> collection, IEqualityComparer<T> comparer) {
        return new HashSet<T>(collection, comparer);
    }

    /// <summary>
    /// Creates a LinkedList&lt;T> from an IEnumerable&lt;T>
    /// </summary>
    /// <typeparam name="T">The type of the elements of collection.</typeparam>
    /// <param name="collection">The collection whose elements are copied to the new set.</param>
    /// <returns>A LinkedList&lt;T> that contains elements from the input sequence.</returns>
    public static LinkedList<T> ToLinkedList<T>(this IEnumerable<T> collection) {
        return new LinkedList<T>(collection);
    }

    /// <summary>
    /// Creates a Queue&lt;T> from an IEnumerable&lt;T>
    /// </summary>
    /// <typeparam name="T">The type of the elements of collection.</typeparam>
    /// <param name="collection">The collection whose elements are copied to the new set.</param>
    /// <returns>A Queue&lt;T> that contains elements from the input sequence.</returns>
    public static Queue<T> ToQueue<T>(this IEnumerable<T> collection) {
        return new Queue<T>(collection);
    }

    /// <summary>
    /// Creates a ReadOnlyCollection&lt;T> from a List&lt;T>
    /// </summary>
    /// <typeparam name="T">The type of the elements</typeparam>
    /// <param name="dictionary">A List to make read-only</param>
    /// <returns>A read-only collection</returns>
    public static ReadOnlyCollection<T> ToReadOnlyCollection<T>(this IList<T> collection) {
        return new ReadOnlyCollection<T>(collection);
    }

    /// <summary>
    /// Creates a ReadOnlyDictionary&lt;TKey, TValue> from a Dictionary&lt;TKey, TValue>
    /// </summary>
    /// <typeparam name="TKey">The type of the key</typeparam>
    /// <typeparam name="TValue">The type of the value</typeparam>
    /// <param name="dictionary">A dictionary to make read-only</param>
    /// <returns>A read-only dictionary</returns>
    public static ReadOnlyDictionary<TKey, TValue> ToReadOnlyDictionary<TKey, TValue>(this IDictionary<TKey, TValue> dictionary) {
        return new ReadOnlyDictionary<TKey, TValue>(dictionary);
    }

    /// <summary>
    /// Creates a SortedDictionary&lt;TKey, TValue> from an IEnumerable&lt;T> according to a specified key selector function.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of source.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by keySelector.</typeparam>
    /// <param name="source">An IEnumerable&lt;T> to create a SortedDictionary&lt;TKey, TValue> from.</param>
    /// <param name="keySelector">A function to extract a key from each element.</param>
    /// <returns>A SortedDictionary&lt;TKey, TValue> that contains keys and values.</returns>
    public static SortedDictionary<TKey, TSource> ToSortedDictionary<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector) {
        return new SortedDictionary<TKey, TSource>(source.ToDictionary(keySelector));
    }

    /// <summary>
    /// Creates a SortedDictionary&lt;TKey, TValue> from an IEnumerable&lt;T> according to a specified key selector function and key comparer.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of source.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by keySelector.</typeparam>
    /// <param name="source">An IEnumerable&lt;T> to create a SortedDictionary&lt;TKey, TValue> from.</param>
    /// <param name="keySelector">A function to extract a key from each element.</param>
    /// <param name="comparer">The default comparer to use for comparing objects.</param>
    /// <returns>A SortedDictionary&lt;TKey, TValue> that contains keys and values.</returns>
    public static SortedDictionary<TKey, TSource> ToSortedDictionary<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer) {
        return new SortedDictionary<TKey, TSource>(source.ToDictionary(keySelector), comparer);
    }

    /// <summary>
    /// Creates a SortedDictionary&lt;TKey, TValue> from an IEnumerable&lt;T> according to a specified key selector and element selector functions.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of source.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by keySelector.</typeparam>
    /// <typeparam name="TElement">The type of the value returned by elementSelector.</typeparam>
    /// <param name="source">An IEnumerable&lt;T> to create a SortedDictionary&lt;TKey, TValue> from.</param>
    /// <param name="keySelector">A function to extract a key from each element.</param>
    /// <param name="elementSelector">A transform function to produce a result element value from each element.</param>
    /// <returns>A SortedDictionary&lt;TKey, TValue> that contains keys and values.</returns>
    public static SortedDictionary<TKey, TElement> ToSortedDictionary<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector) {
        SortedDictionary<TKey, TElement> output = new SortedDictionary<TKey, TElement>();
        foreach (TSource item in source) {
            output.Add(keySelector(item), elementSelector(item));
        }
        return output;
    }

    /// <summary>
    /// Creates a SortedDictionary&lt;TKey, TValue> from an IEnumerable&lt;T> according to a specified key selector function, a comparer, and an element selector function.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of source.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by keySelector.</typeparam>
    /// <typeparam name="TElement">The type of the value returned by elementSelector.</typeparam>
    /// <param name="source">An IEnumerable&lt;T> to create a SortedDictionary&lt;TKey, TValue> from.</param>
    /// <param name="keySelector">A function to extract a key from each element.</param>
    /// <param name="elementSelector">A transform function to produce a result element value from each element.</param>
    /// <param name="comparer">The default comparer to use for comparing objects.</param>
    /// <returns>A SortedDictionary&lt;TKey, TValue> that contains keys and values.</returns>
    public static SortedDictionary<TKey, TElement> ToSortedDictionary<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IComparer<TKey> comparer) {
        SortedDictionary<TKey, TElement> output = new SortedDictionary<TKey, TElement>(comparer);
        foreach (TSource item in source) {
            output.Add(keySelector(item), elementSelector(item));
        }
        return output;
    }

    /// <summary>
    /// Creates a SortedList&lt;TKey, TValue> from an IEnumerable&lt;T> according to a specified key selector function.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of source.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by keySelector.</typeparam>
    /// <param name="source">An IEnumerable&lt;T> to create a SortedList&lt;TKey, TValue> from.</param>
    /// <param name="keySelector">A function to extract a key from each element.</param>
    /// <returns>A SortedList&lt;TKey, TValue> that contains keys and values.</returns>
    public static SortedList<TKey, TSource> ToSortedList<TKey, TSource>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector) {
        return new SortedList<TKey, TSource>(source.ToDictionary(keySelector));
    }

    /// <summary>
    /// Creates a SortedList&lt;TKey, TValue> from an IEnumerable&lt;T> according to a specified key selector function and key comparer.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of source.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by keySelector.</typeparam>
    /// <param name="source">An IEnumerable&lt;T> to create a SortedList&lt;TKey, TValue> from.</param>
    /// <param name="keySelector">A function to extract a key from each element.</param>
    /// <param name="comparer">The default comparer to use for comparing objects.</param>
    /// <returns>A SortedList&lt;TKey, TValue> that contains keys and values.</returns>
    public static SortedList<TKey, TSource> ToSortedList<TKey, TSource>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer) {
        return new SortedList<TKey, TSource>(source.ToDictionary(keySelector), comparer);
    }

    /// <summary>
    /// Creates a SortedList&lt;TKey, TValue> from an IEnumerable&lt;T> according to a specified key selector and element selector functions.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of source.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by keySelector.</typeparam>
    /// <typeparam name="TElement">The type of the value returned by elementSelector.</typeparam>
    /// <param name="source">An IEnumerable&lt;T> to create a SortedList&lt;TKey, TValue> from.</param>
    /// <param name="keySelector">A function to extract a key from each element.</param>
    /// <param name="elementSelector">A transform function to produce a result element value from each element.</param>
    /// <returns>A SortedList&lt;TKey, TValue> that contains keys and values.</returns>
    public static SortedList<TKey, TElement> ToSortedList<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector) {
        SortedList<TKey, TElement> output = new SortedList<TKey, TElement>();
        foreach (TSource item in source) {
            output.Add(keySelector(item), elementSelector(item));
        }
        return output;
    }

    /// <summary>
    /// Creates a SortedList&lt;TKey, TValue> from an IEnumerable&lt;T> according to a specified key selector function, a comparer, and an element selector function.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of source.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by keySelector.</typeparam>
    /// <typeparam name="TElement">The type of the value returned by elementSelector.</typeparam>
    /// <param name="source">An IEnumerable&lt;T> to create a SortedList&lt;TKey, TValue> from.</param>
    /// <param name="keySelector">A function to extract a key from each element.</param>
    /// <param name="elementSelector">A transform function to produce a result element value from each element.</param>
    /// <param name="comparer">The default comparer to use for comparing objects.</param>
    /// <returns>A SortedList&lt;TKey, TValue> that contains keys and values.</returns>
    public static SortedList<TKey, TElement> ToSortedList<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IComparer<TKey> comparer) {
        SortedList<TKey, TElement> output = new SortedList<TKey, TElement>(comparer);
        foreach (TSource item in source) {
            output.Add(keySelector(item), elementSelector(item));
        }
        return output;
    }

    /// <summary>
    /// Creates a SortedSet&lt;T> from an IEnumerable&lt;T>
    /// </summary>
    /// <typeparam name="T">The type of the elements of collection.</typeparam>
    /// <param name="collection">The IEnumerable&lt;T> to create a SortedSet&lt;T> from.</param>
    /// <returns>A SortedSet&lt;T> that contains elements from the input sequence.</returns>
    public static SortedSet<T> ToSortedSet<T>(this IEnumerable<T> collection) {
        return new SortedSet<T>(collection);
    }

    /// <summary>
    /// Creates a SortedSet&lt;T> from an IEnumerable&lt;T>
    /// </summary>
    /// <typeparam name="T">The type of the elements of collection.</typeparam>
    /// <param name="collection">The IEnumerable&lt;T> to create a SortedSet&lt;T> from.</param>
    /// <param name="comparer">The default comparer to use for comparing objects.</param>
    /// <returns>A SortedSet&lt;T> that contains elements from the input sequence.</returns>
    public static SortedSet<T> ToSortedSet<T>(this IEnumerable<T> collection, IComparer<T> comparer) {
        return new SortedSet<T>(collection, comparer);
    }

    /// <summary>
    /// Creates a Stack&lt;T> from an IEnumerable&lt;T>
    /// </summary>
    /// <typeparam name="T">The type of the elements of collection.</typeparam>
    /// <param name="collection">The collection whose elements are copied to the new set.</param>
    /// <returns>A Stack&lt;T> that contains elements from the input sequence.</returns>
    public static Stack<T> ToStack<T>(this IEnumerable<T> collection) {
        return new Stack<T>(collection);
    }

    #endregion


}