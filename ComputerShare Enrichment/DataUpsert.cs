﻿using System;
using System.Collections.Generic;
using System.Linq;
using SforceApi;
using SforceApi.Sforce;
using NLog;

namespace ComputerShare_Enrichment {
    public class DataUpsert : IDisposable {

        private SforceConnection connection;

        private const string datafeedCode = "DF.CPU";

        private const string divisionId = "PEN.DIV.B2C";

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private bool disposedValue;

        public DataUpsert(string username, string password, string token) {
            connection = new SforceConnection();
            connection.Open(username, password, token);
        }

        protected virtual void Dispose(bool disposing) {
            if (!disposedValue) {
                if (disposing) {
                }
                connection.Close();
                disposedValue = true;
            }
        }

        void IDisposable.Dispose() {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public void UpsertContact(IEnumerable<EnrichedData> data) {
            if (data == null) throw new ArgumentNullException("data");
            if (data.Count() == 0) throw new ArgumentException("data has no records");

            var minHolderOnDates = data
                .Where(g => g.HolderOnDate.HasValue)
                .GroupBy(g => g.GenericExternalId)
                .ToDictionary(k => k.Key, v => v.Min(e => e.HolderOnDate));

            var maxDataDate = data.Max(e => e.AsAtDate);

            var openAccounts = data
                .Where(e => e.AsAtDate == maxDataDate && e.Units > 0)
                .Select(e => e.GenericExternalId)
                .Distinct()
                .ToHashSet();

            var consolidatedData = data
                .GroupBy(g => g.GenericExternalId)
                .SelectMany(g => g
                    .OrderByDescending(grp => grp.AsAtDate)
                    .ThenByDescending(grp => grp.MarketValue)
                    .Take(1)
                )
                .OrderBy(e => e.HoldingType) // insert new records first so that new contact loading errors can immediately be detected and rectified.
                .ToList();

            var records = new List<sObject>();
            foreach (var e in consolidatedData) {

                var record = new Contact();

                record.cloupra__External_Id__c = e.AccountExternalId;

                var division = new Account();
                division.cloupra__External_Id__c = divisionId;
                record.cloupra__Primary_Division__r = division;

                var personSegment = new cloupra__Category__c();
                personSegment.cloupra__Code__c = "PSEGINVESTORACC";
                record.cloupra__Person_Segment__r = personSegment;

                var stage = new cloupra__Category__c();
                stage.cloupra__Code__c = openAccounts.Contains(e.GenericExternalId) ? "PSACTIVE" : "PSINACTIVE";
                record.cloupra__Stage__r = stage;

                record.LastName = e.AccountName.Length > 80 ? e.AccountName.Substring(0, 80) : e.AccountName; 

                if (e.GenericExternalId.Length > 50) {
                    throw new ArgumentException("cloupra__Business_Number__c has a maximum length of 50 which is exceeded by e.GenericExternalId of " + e.GenericExternalId);
                }
                record.cloupra__Business_Number__c = e.GenericExternalId;

                record.LeadSource = "Computershare";

                record.InvestorAccountType__c = e.AccountType.ToString();

                if (minHolderOnDates.ContainsKey(e.GenericExternalId)) {
                    record.Birthdate = minHolderOnDates[e.GenericExternalId];
                    record.BirthdateSpecified = true;
                }

                record.BrokerName__c = !string.IsNullOrWhiteSpace(e.BrokerName) && e.BrokerName.Length > 255 ? e.BrokerName.Substring(0, 255) : e.BrokerName;

                record.IsAdvised__c = e.HasPlanner;
                record.IsAdvised__cSpecified = true;

                record.Platform__c = e.Platform;

                record.Email = e.Email;
                
                record.HasOptedOutOfEmail = e.EmailOptOut;
                record.HasOptedOutOfEmailSpecified = true;

                record.Phone = e.WorkPhone;
                record.MobilePhone = e.MobilePhone;
                record.HomePhone = e.HomePhone;

                record.DoNotCall = e.DoNotCall;
                record.DoNotCallSpecified = true;

                record.MailingStreet = e.PostalStreet;
                record.MailingCity = e.PostalCity;
                record.MailingState = e.PostalState;
                record.MailingPostalCode = e.PostalCode;
                record.MailingCountry = e.PostalCountry;

                string addresseeName = string.Join(" & ", e.People.Count >= 1 ? e.People.Select(v => v.FirstName) : e.Entities.Select(v => v.Name));
                record.cloupra__Addressee_Name__c = addresseeName.Length > 255 ? addresseeName.Substring(0, 255) : addresseeName;

                record.ComputershareCommunicationPreference__c = e.ContactPreference.ToString();
                
                records.Add(record);
            }
            logger.Info("Preparing to upsert {0} contact records.", records.Count);
            var result = connection.Upsert(records, "cloupra__External_Id__c", 75);
            var comment = string.Format("Upserted {0} contact records.", records.Count);
            logger.Info(comment);
            PractifiCommon.LogToPractifi(connection, System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, result, comment);
        }

        public void UpsertPortfolioHolding(IEnumerable<EnrichedData> data) {
            if (data == null) throw new ArgumentNullException("data");
            if (data.Count() == 0) throw new ArgumentException("data has no records");
            var records = new List<sObject>();
            foreach (var e in data) {

                var record = new cloupra__Portfolio_Holding__c();

                record.cloupra__ExternalId__c = e.AssetExternalId;

                var person = new Contact();
                person.cloupra__External_Id__c = e.AccountExternalId;
                record.cloupra__Person__r = person;

                var dataFeed = new cloupra__Data_Feed__c();
                dataFeed.cloupra__Code__c = datafeedCode;
                record.cloupra__Data_Feed__r = dataFeed;

                var provider = new Account();
                provider.cloupra__External_Id__c = e.AsxCode;
                record.cloupra__Provider__r = provider;

                var division = new Account();
                division.cloupra__External_Id__c = divisionId;
                record.cloupra__Primary_Division__r = division;

                var portfolioHoldingType = new cloupra__Category__c();
                portfolioHoldingType.cloupra__Code__c = "PFLOASSET";
                record.cloupra__Portfolio_Holding_Type__r = portfolioHoldingType;

                record.cloupra__Valuation_Date__c = e.AsAtDate;
                record.cloupra__Valuation_Date__cSpecified = true;

                record.Inflows__c = Convert.ToDouble(Math.Abs(e.MarketValueBought));
                record.Inflows__cSpecified = true;

                record.Outflows__c = Convert.ToDouble(Math.Abs(e.MarketValueSold));
                record.Outflows__cSpecified = true;

                record.cloupra__Current_Value__c = Convert.ToDouble(e.MarketValue);
                record.cloupra__Current_Value__cSpecified = true;

                record.cloupra__Under_Advice__c = true;
                record.cloupra__Under_Advice__cSpecified = true;

                records.Add(record);
            }
            logger.Info("Preparing to upsert {0} portfolio holding records.", records.Count);
            var result = connection.Upsert(records, "cloupra__ExternalId__c", 200);
            var comment = string.Format("Upserted {0} portfolio holding records.", records.Count);
            logger.Info(comment);
            PractifiCommon.LogToPractifi(connection, System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, result, comment);
        }

        public void UpsertDataFeed(IEnumerable<EnrichedData> data) {
            if (data == null) throw new ArgumentNullException("data");
            if (data.Count() == 0) throw new ArgumentException("data has no records");
            var record = new cloupra__Data_Feed__c();
            record.cloupra__Code__c = datafeedCode;
            record.LatestValuationDate__c = data.Max(e => e.AsAtDate);
            record.LatestValuationDate__cSpecified = true;
            logger.Info("Preparing to upsert datafeed {0} to {1}.", record.cloupra__Code__c, record.LatestValuationDate__c.Value.ToString("yyyy-MM-dd"));
            var result = connection.Upsert(record, "cloupra__Code__c");
            var comment = string.Format("Upserted datafeed {0} valuation date to {1}.", record.cloupra__Code__c, record.LatestValuationDate__c.Value.ToString("yyyy-MM-dd"));
            logger.Info(comment);
            PractifiCommon.LogToPractifi(connection, System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, result, comment);
        }

    }
}
