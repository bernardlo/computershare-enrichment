﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace ComputerShare_Enrichment {

    [DebuggerDisplay("[{ExternalId}] {AccountName}")]
    public class EnrichedData {

        // The primary key for a collection of enriched data is { AsAtDate, AsxCode, Hin }

        /// <summary>
        /// Date of the source report
        /// </summary>
        public DateTime AsAtDate { get; set; }

        /// <summary>
        /// ASX Ticker
        /// </summary>
        public string AsxCode { get; set; }

        /// <summary>
        /// The Holder Identification Number or the Stock Reference Number.
        /// This is the account number.
        /// A HIN starts with "X", and can hold shares from multiple AsxCodes and is unique across AsxCodes.
        /// A SRN starts with "I", and can hold shares from a single AsxCode and is not unique across AsxCodes.
        /// </summary>
        public string Hin { get; set; }

        /// <summary>
        /// As SRNs are not necessarily unique across AsxCodes an Salesforce ExternalId is created by appending the
        /// AsxCode at the end of an Holder Identifier that is a SRN. If the holder identifier is a HIN it is left unchanged.
        /// </summary>
        public string GenericExternalId { get => Hin.StartsWith("I", StringComparison.OrdinalIgnoreCase) ? string.Join(".", Hin, AsxCode) : Hin; }

        public string AccountExternalId { get => "CPU.ACC." + GenericExternalId; }

        public string AssetExternalId { get => string.Join(".", "CPU", "PRT", AsAtDate.ToString("yyMMdd"), Hin, AsxCode); }

        /// <summary>
        /// The client name is a concatenation of the account's clients (being people and/or entities/companies).
        /// </summary>
        public string ClientName { get; set; }

        /// <summary>
        /// The AccountName which is a concatenation of the ClientName and the account designation (if any).
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// An account can have up to 3 people/entities
        /// </summary>
        public List<Person> People { get; set; }

        /// <summary>
        /// An account can have up to 3 people/entities
        /// Computershare calls entities companies.
        /// </summary>
        public List<Entity> Entities { get; set; }

        /// <summary>
        /// The account's address.
        /// A concatenation (by new line) of each of CPU's address components which are randomly split
        /// across multiple fields. Loaded into Practifi as the postal street address which can then be
        /// subsequently loaded and parsed through the Google Maps API to split it out into SFDC components.
        /// </summary>
        public string CombinedPostalAddress { get; set; }

        /// <summary>
        /// Postal street of the account
        /// </summary>
        public string PostalStreet { get; set; }

        /// <summary>
        /// Postal city of the account
        /// </summary>
        public string PostalCity { get; set; }

        /// <summary>
        /// Postal state of the account
        /// </summary>
        public string PostalState { get; set; }

        /// <summary>
        /// Postal code of the account
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// Engligh name of the account's postal country
        /// </summary>
        public string PostalCountry { get; set; }

        /// <summary>
        /// The date the {HIN, BrokerCode} first became a holder in the stock {AsxCode}
        /// </summary>
        public DateTime? HolderOnDate { get; set; }

        /// <summary>
        /// Computershare's internal broker code.
        /// 
        /// This is the last broker the HIN used to trade in the AsxCode.
        /// If you stored it in the assets and cashflows object then you'd see the history.
        /// Alternatively, if we stored it under the account, we would just have access to the most recently used broker.
        /// 
        /// I think you can treat a broker like a platform. You will be keeping platforms in your Service object?
        /// </summary>
        public string BrokerCode { get; set; }

        /// <summary>
        /// Broker name. See comments for BrokerCode.
        /// </summary>
        public string BrokerName { get; set; }

        /// <summary>
        /// Email, phone or postage or null (no preference) for the { HIN, AsxCode } combination
        /// </summary>
        public ContactPreference? ContactPreference { get; set; }

        /// <summary>
        /// Email address for the HIN
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Whether this { HIN, AsxCode } combination has opted out of emails
        /// </summary>
        public bool EmailOptOut { get => ContactPreference.HasValue && ContactPreference.Value == ComputerShare_Enrichment.ContactPreference.DoNotContact; }

        /// <summary>
        /// Home phone number (of the account) in international format: +61 2 8524 9900. Can also contain a mobile number.
        /// </summary>
        public string HomePhone { get; set; }

        /// <summary>
        /// Work phone number (of the account) in international format: +61 2 8524 9900. Can also contain a mobile number.
        /// </summary>
        public string WorkPhone { get; set; }

        /// <summary>
        /// Mobile phone number (of the account) in international format: +61 403 123 456.
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// Whether this { HIN, AsxCode } combination has opted out of emails
        /// </summary>
        public bool DoNotCall { get => ContactPreference.HasValue && ContactPreference.Value == ComputerShare_Enrichment.ContactPreference.DoNotContact; }

        /// <summary>
        /// Number of units or shares held
        /// </summary>
        public decimal Units { get; set; }

        /// <summary>
        /// The price of each unit or share on the AsAtDate
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// The dollar value held in the stock
        /// </summary>
        public decimal MarketValue { get => Units * Price; }

        /// <summary>
        /// The units traded. This will be approximated by looking at the difference in unit balances between two ComputerShare files.
        /// </summary>
        public decimal UnitsTraded { get; set; }

        /// <summary>
        /// If UnitsTraded is positive then return it, else 0.
        /// </summary>
        public decimal UnitsBought { get => UnitsTraded > 0 ? UnitsTraded : 0m; }

        /// <summary>
        /// If UnitsTraded is negative then return it, else 0.
        /// </summary>
        public decimal UnitsSold { get => UnitsTraded < 0 ? UnitsTraded : 0m; }

        /// <summary>
        /// A user supplied Volume Weighted Average Price used to approximate the dollar value traded over the AsAtDate's month. 
        /// </summary>
        public decimal Vwap { get; set; }

        /// <summary>
        /// The approximate dollar value traded over the AsAtDate's month.
        /// </summary>
        public decimal MarketValueTraded { get => UnitsTraded * Vwap; }

        /// <summary>
        /// If MarketValueTraded is positive then return it, else 0.
        /// </summary>
        public decimal MarketValueBought { get => UnitsBought * Vwap; }

        /// <summary>
        /// If MarketValueTraded is negative then return it, else 0.
        /// </summary>
        public decimal MarketValueSold { get => UnitsSold * Vwap; }

        /// <summary>
        /// An approximated account type based on certain strings of text appearing in the AccountName.
        /// </summary>
        public AccountType AccountType { get; set; }

        public HoldingType HoldingType {
            get {
                if (Units == 0) {
                    return HoldingType.Closed;
                } else if (Units == UnitsBought) {
                    return HoldingType.New;
                } else {
                    return HoldingType.Existing;
                }
            }
        }

        /// <summary>
        /// Whether the account has an adviser. This is the extent of our current analysis in Excel.
        /// Once the data is in Practifi we would attempt to join to actual advisers in the DB.
        /// </summary>
        public bool HasPlanner { get; set; }
        
        /// <summary>
        /// The IDPS / platform the investment is part of.
        /// </summary>
        public string Platform { get; set; }

    }

    [DebuggerDisplay("[{Sequence}] {Name}")]
    public class Person : Client {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public override string Name { get => string.Join(" ", new string[] { FirstName, MiddleName, LastName }.Where(s => !string.IsNullOrEmpty(s)).Select(s => s.Trim())); }
    }

    [DebuggerDisplay("[{Sequence}] {Name}")]
    public class Entity : Client {
    }

    [DebuggerDisplay("[{Sequence}] {Name}")]
    public class Client {
        public string ExternalId { get; set; }
        /// <summary>
        /// An account can have up to 3 people/entities. The sequence represents the order of appearance and is consistent over time.
        /// </summary>
        public int Sequence { get; set; }
        /// <summary>
        /// Clients' external ID is comprised of the string "CLI", the HIN, then the sequence (and is joined by periods).
        /// </summary>
        public string SubClientExternalId { get => string.Join(".", "CLI", ExternalId, Sequence); }
        public virtual string Name { get; set; }
        public bool PrimaryContact { get => Sequence == 1; }
    }

}
