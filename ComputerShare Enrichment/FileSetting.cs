﻿using System;
using System.Globalization;
using CsvHelper.Configuration;

namespace ComputerShare_Enrichment {

    public class FileSetting {
        public DateTime AsAtDate { get; set; }
        public string AsxCode { get; set; }
        public decimal Price { get; set; }
        public decimal Vwap { get; set; }
        public string CurrFilePath { get; set; }
        public string PrevFilePath { get; set; }
    }

    public class FileSettingMap : ClassMap<FileSetting> {
        public FileSettingMap () {
            Map(m => m.AsAtDate).ConvertUsing(row => DateTime.ParseExact(row.GetField("AsAtDate"), "yyyy-MM-dd", CultureInfo.InvariantCulture));
            Map(m => m.AsxCode);
            Map(m => m.Price);
            Map(m => m.Vwap);
            Map(m => m.CurrFilePath);
            Map(m => m.PrevFilePath);
        }
    }

}
