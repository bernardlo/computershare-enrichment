﻿using System.Collections.ObjectModel;
using System.Linq;

namespace ComputerShare_Enrichment {
    public class CpuDataCollection : KeyedCollection<string, CpuData> {

        /// <summary>
        /// Extracts the key from the specified element.
        /// </summary>
        /// <param name="item">The element from which to extract the key.</param>
        /// <returns>The key for the specified element.</returns>
        protected override string GetKeyForItem (CpuData item) {
            return item.Hin;
        }

        /// <summary>
        /// Returns a value indicating whether this instance is equal to another instance.
        /// </summary>
        public virtual bool Equals (CpuDataCollection other) {
            if (other == null) {
                return false;
            }
            return this.SequenceEqual(other as KeyedCollection<string, CpuData>);
        }

        /// <summary>
        /// Returns a value indicating whether this instance is equal to another object.
        /// </summary>
        public override bool Equals (object obj) {
            if (obj == null || GetType() != obj.GetType()) {
                return false;
            }
            CpuDataCollection other = (CpuDataCollection)obj;
            return Equals(other);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        public override int GetHashCode () {
            unchecked {
                int hash = (int)2166136261;
                foreach (CpuData e in this.Items) {
                    if (e != null) {
                        hash = hash * 16777619 ^ e.GetHashCode();
                    }
                }
                return hash;
            }
        }
    }
}
