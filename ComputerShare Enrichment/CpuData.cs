﻿using System;
using System.Diagnostics;

namespace ComputerShare_Enrichment {

    [DebuggerDisplay("[{Hin}]")]
    public class CpuData : IComparable<CpuData>, IEquatable<CpuData>, ICloneable {
        public string Hin { get; set; }
        public string Person1_Surname { get; set; }
        public string Person1_Name1 { get; set; }
        public string Person2_Name1 { get; set; }
        public string Person3_Name1 { get; set; }
        public string CompanyName1 { get; set; }
        public string Person2_Surname { get; set; }
        public string Person1_Name2 { get; set; }
        public string Person2_Name2 { get; set; }
        public string Person3_Name2 { get; set; }
        public string CompanyName2 { get; set; }
        public string Person3_Surname { get; set; }
        public string Person1_Name3 { get; set; }
        public string Person2_Name3 { get; set; }
        public string Person3_Name3 { get; set; }
        public string CompanyName3 { get; set; }
        public string AccountDesignation { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string AddressLine5 { get; set; }
        public string AddressLine6 { get; set; }
        public string AddressLine7 { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public DateTime HolderOnDate { get; set; }
        public string BrokerCode { get; set; }
        public string BrokerName { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string ContactPreference { get; set; }
        public decimal Units { get; set; }

        public object Clone () {
            return MemberwiseClone();
        }

        /// <summary>
        /// Compares this instance with a specified <see cref="CpuData"/> object and indicates whether
        /// this instance precedes, follows, or appears in the same position in the sort order.
        /// </summary>
        /// <param name="other">The <see cref="CpuData"/> to compare with this instance.</param>
        public int CompareTo (CpuData other) {
            if (other == null) {
                return 1;
            }
            if (this.Hin != other.Hin) {
                return this.Hin.CompareTo(other.Hin);
            } else if (this.Person1_Surname != other.Person1_Surname) {
                return this.Person1_Surname.CompareTo(other.Person1_Surname);
            } else if (this.Person1_Name1 != other.Person1_Name1) {
                return this.Person1_Name1.CompareTo(other.Person1_Name1);
            } else if (this.Person2_Name1 != other.Person2_Name1) {
                return this.Person2_Name1.CompareTo(other.Person2_Name1);
            } else if (this.Person3_Name1 != other.Person3_Name1) {
                return this.Person3_Name1.CompareTo(other.Person3_Name1);
            } else if (this.CompanyName1 != other.CompanyName1) {
                return this.CompanyName1.CompareTo(other.CompanyName1);
            } else if (this.Person2_Surname != other.Person2_Surname) {
                return this.Person2_Surname.CompareTo(other.Person2_Surname);
            } else if (this.Person1_Name2 != other.Person1_Name2) {
                return this.Person1_Name2.CompareTo(other.Person1_Name2);
            } else if (this.Person2_Name2 != other.Person2_Name2) {
                return this.Person2_Name2.CompareTo(other.Person2_Name2);
            } else if (this.Person3_Name2 != other.Person3_Name2) {
                return this.Person3_Name2.CompareTo(other.Person3_Name2);
            } else if (this.CompanyName2 != other.CompanyName2) {
                return this.CompanyName2.CompareTo(other.CompanyName2);
            } else if (this.Person3_Surname != other.Person3_Surname) {
                return this.Person3_Surname.CompareTo(other.Person3_Surname);
            } else if (this.Person1_Name3 != other.Person1_Name3) {
                return this.Person1_Name3.CompareTo(other.Person1_Name3);
            } else if (this.Person2_Name3 != other.Person2_Name3) {
                return this.Person2_Name3.CompareTo(other.Person2_Name3);
            } else if (this.Person3_Name3 != other.Person3_Name3) {
                return this.Person3_Name3.CompareTo(other.Person3_Name3);
            } else if (this.CompanyName3 != other.CompanyName3) {
                return this.CompanyName3.CompareTo(other.CompanyName3);
            } else if (this.AccountDesignation != other.AccountDesignation) {
                return this.AccountDesignation.CompareTo(other.AccountDesignation);
            } else if (this.AddressLine1 != other.AddressLine1) {
                return this.AddressLine1.CompareTo(other.AddressLine1);
            } else if (this.AddressLine2 != other.AddressLine2) {
                return this.AddressLine2.CompareTo(other.AddressLine2);
            } else if (this.AddressLine3 != other.AddressLine3) {
                return this.AddressLine3.CompareTo(other.AddressLine3);
            } else if (this.AddressLine4 != other.AddressLine4) {
                return this.AddressLine4.CompareTo(other.AddressLine4);
            } else if (this.AddressLine5 != other.AddressLine5) {
                return this.AddressLine5.CompareTo(other.AddressLine5);
            } else if (this.AddressLine6 != other.AddressLine6) {
                return this.AddressLine6.CompareTo(other.AddressLine6);
            } else if (this.AddressLine7 != other.AddressLine7) {
                return this.AddressLine7.CompareTo(other.AddressLine7);
            } else if (this.PostalCode != other.PostalCode) {
                return this.PostalCode.CompareTo(other.PostalCode);
            } else if (this.Country != other.Country) {
                return this.Country.CompareTo(other.Country);
            } else if (this.HolderOnDate != other.HolderOnDate) {
                return this.HolderOnDate.CompareTo(other.HolderOnDate);
            } else if (this.BrokerCode != other.BrokerCode) {
                return this.BrokerCode.CompareTo(other.BrokerCode);
            } else if (this.BrokerName != other.BrokerName) {
                return this.BrokerName.CompareTo(other.BrokerName);
            } else if (this.HomePhone != other.HomePhone) {
                return this.HomePhone.CompareTo(other.HomePhone);
            } else if (this.WorkPhone != other.WorkPhone) {
                return this.WorkPhone.CompareTo(other.WorkPhone);
            } else if (this.MobilePhone != other.MobilePhone) {
                return this.MobilePhone.CompareTo(other.MobilePhone);
            } else if (this.Email != other.Email) {
                return this.Email.CompareTo(other.Email);
            } else if (this.ContactPreference != other.ContactPreference) {
                return this.ContactPreference.CompareTo(other.ContactPreference);
            } else {
                return this.Units.CompareTo(other.Units);
            }
        }

        /// <summary>
        /// Returns a value indicating whether this instance is equal to another instance.
        /// </summary>
        public bool Equals (CpuData other) {
            if (other == null) {
                return false;
            }
            return
                other.Hin.Equals(this.Hin) &&
                other.Person1_Surname.Equals(this.Person1_Surname) &&
                other.Person1_Name1.Equals(this.Person1_Name1) &&
                other.Person2_Name1.Equals(this.Person2_Name1) &&
                other.Person3_Name1.Equals(this.Person3_Name1) &&
                other.CompanyName1.Equals(this.CompanyName1) &&
                other.Person2_Surname.Equals(this.Person2_Surname) &&
                other.Person1_Name2.Equals(this.Person1_Name2) &&
                other.Person2_Name2.Equals(this.Person2_Name2) &&
                other.Person3_Name2.Equals(this.Person3_Name2) &&
                other.CompanyName2.Equals(this.CompanyName2) &&
                other.Person3_Surname.Equals(this.Person3_Surname) &&
                other.Person1_Name3.Equals(this.Person1_Name3) &&
                other.Person2_Name3.Equals(this.Person2_Name3) &&
                other.Person3_Name3.Equals(this.Person3_Name3) &&
                other.CompanyName3.Equals(this.CompanyName3) &&
                other.AccountDesignation.Equals(this.AccountDesignation) &&
                other.AddressLine1.Equals(this.AddressLine1) &&
                other.AddressLine2.Equals(this.AddressLine2) &&
                other.AddressLine3.Equals(this.AddressLine3) &&
                other.AddressLine4.Equals(this.AddressLine4) &&
                other.AddressLine5.Equals(this.AddressLine5) &&
                other.AddressLine6.Equals(this.AddressLine6) &&
                other.AddressLine7.Equals(this.AddressLine7) &&
                other.PostalCode.Equals(this.PostalCode) &&
                other.Country.Equals(this.Country) &&
                other.HolderOnDate.Equals(this.HolderOnDate) &&
                other.BrokerCode.Equals(this.BrokerCode) &&
                other.BrokerName.Equals(this.BrokerName) &&
                other.HomePhone.Equals(this.HomePhone) &&
                other.WorkPhone.Equals(this.WorkPhone) &&
                other.MobilePhone.Equals(this.MobilePhone) &&
                other.Email.Equals(this.Email) &&
                other.ContactPreference.Equals(this.ContactPreference) &&
                other.Units.Equals(this.Units);
        }

        /// <summary>
        /// Returns a value indicating whether this instance is equal to another object.
        /// </summary>
        public override bool Equals (object obj) {
            if (obj == null || GetType() != obj.GetType()) {
                return false;
            }
            CpuData other = (CpuData)obj;
            return Equals(other);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        public override int GetHashCode () {
            unchecked {
                int hash = (int)2166136261;
                int mult = 16777619;
                if (Hin != null) hash = hash * mult ^ Hin.GetHashCode();
                if (Person1_Surname != null) hash = hash * mult ^ Person1_Surname.GetHashCode();
                if (Person1_Name1 != null) hash = hash * mult ^ Person1_Name1.GetHashCode();
                if (Person2_Name1 != null) hash = hash * mult ^ Person2_Name1.GetHashCode();
                if (Person3_Name1 != null) hash = hash * mult ^ Person3_Name1.GetHashCode();
                if (CompanyName1 != null) hash = hash * mult ^ CompanyName1.GetHashCode();
                if (Person2_Surname != null) hash = hash * mult ^ Person2_Surname.GetHashCode();
                if (Person1_Name2 != null) hash = hash * mult ^ Person1_Name2.GetHashCode();
                if (Person2_Name2 != null) hash = hash * mult ^ Person2_Name2.GetHashCode();
                if (Person3_Name2 != null) hash = hash * mult ^ Person3_Name2.GetHashCode();
                if (CompanyName2 != null) hash = hash * mult ^ CompanyName2.GetHashCode();
                if (Person3_Surname != null) hash = hash * mult ^ Person3_Surname.GetHashCode();
                if (Person1_Name3 != null) hash = hash * mult ^ Person1_Name3.GetHashCode();
                if (Person2_Name3 != null) hash = hash * mult ^ Person2_Name3.GetHashCode();
                if (Person3_Name3 != null) hash = hash * mult ^ Person3_Name3.GetHashCode();
                if (CompanyName3 != null) hash = hash * mult ^ CompanyName3.GetHashCode();
                if (AccountDesignation != null) hash = hash * mult ^ AccountDesignation.GetHashCode();
                if (AddressLine1 != null) hash = hash * mult ^ AddressLine1.GetHashCode();
                if (AddressLine2 != null) hash = hash * mult ^ AddressLine2.GetHashCode();
                if (AddressLine3 != null) hash = hash * mult ^ AddressLine3.GetHashCode();
                if (AddressLine4 != null) hash = hash * mult ^ AddressLine4.GetHashCode();
                if (AddressLine5 != null) hash = hash * mult ^ AddressLine5.GetHashCode();
                if (AddressLine6 != null) hash = hash * mult ^ AddressLine6.GetHashCode();
                if (AddressLine7 != null) hash = hash * mult ^ AddressLine7.GetHashCode();
                if (PostalCode != null) hash = hash * mult ^ PostalCode.GetHashCode();
                if (Country != null) hash = hash * mult ^ Country.GetHashCode();
                hash = hash * mult ^ HolderOnDate.GetHashCode();
                if (BrokerCode != null) hash = hash * mult ^ BrokerCode.GetHashCode();
                if (BrokerName != null) hash = hash * mult ^ BrokerName.GetHashCode();
                if (HomePhone != null) hash = hash * mult ^ HomePhone.GetHashCode();
                if (WorkPhone != null) hash = hash * mult ^ WorkPhone.GetHashCode();
                if (MobilePhone != null) hash = hash * mult ^ MobilePhone.GetHashCode();
                if (Email != null) hash = hash * mult ^ Email.GetHashCode();
                if (ContactPreference != null) hash = hash * mult ^ ContactPreference.GetHashCode();
                hash = hash * mult ^ Units.GetHashCode();
                return hash;
            }
        }

    }
}